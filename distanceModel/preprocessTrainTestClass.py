import os
from baseZhang import if_no_create_it

dir_path = "../../data/doors_stairs/dis_video/Door-3-10ft.MTS"


def rename_train_test_for_distance(data_dir):
    for root, dirs, files in os.walk(data_dir):
        for jpg_file in files:
            jpg_path = os.path.join(root, jpg_file)
            if '.jpg' in jpg_path:
                folder = jpg_path.split('/')[-2]
                distance = jpg_path.split('_')[-2].split('-')[-1][:-2]

                if '00' in jpg_path:
                    distance = 'test/' + distance
                else:
                    distance = 'train/' + distance
                new_jpg_path = jpg_path.replace(folder, distance)
                if_no_create_it(new_jpg_path)
                os.rename(jpg_path, new_jpg_path)  # TODO bug
    return 0


# rename_train_test_for_distance("../../data/doors_stairs/newer/")
for root, dirs, files in os.walk("../../data/doors_stairs/door_stair_train"):
    for jpg_file in files:
        jpg_path = os.path.join(root, jpg_file)
        if '.jpg' in jpg_path:
            door_stair = os.path.basename(jpg_path).split('-')[0]
            new_name = "../../data/doors_stairs/door_stair_train/" + door_stair + '/' + os.path.basename(jpg_path)
            if_no_create_it(new_name)
            os.rename(jpg_path, new_name)
