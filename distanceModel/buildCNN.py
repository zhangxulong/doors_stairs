from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
import numpy as np
from keras.preprocessing.image import ImageDataGenerator

K.set_image_dim_ordering('th')
SEED = 1007
np.random.seed(SEED)
DATA_TRAIN = "../../data/doors_stairs/dist_train"
DATA_TEST = "../../data/doors_stairs/dist_test"
model = Sequential()
model.add(Conv2D(32, 3, 3, input_shape=(3, 150, 150)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

# the model so far outputs 3D feature maps (height, width, features)

model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(9))
model.add(Activation('sigmoid'))

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

batch_size = 4

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

# this is a generator that will read pictures found in
# subfolers of 'data/train', and indefinitely generate
# batches of augmented image data
train_generator = train_datagen.flow_from_directory(
    DATA_TRAIN,  # this is the target directory
    target_size=(150, 150),  # all images will be resized to 150x150
    batch_size=batch_size,
    class_mode='categorical')  # since we use binary_crossentropy loss, we need binary labels

# this is a similar generator, for validation data
validation_generator = test_datagen.flow_from_directory(
    DATA_TEST,
    target_size=(150, 150),
    batch_size=batch_size,
    class_mode='categorical')
early_stop = EarlyStopping(monitor='val_acc', patience=3)
model.fit_generator(
    train_generator,
    4000 // batch_size,
    50,
    validation_data=validation_generator,
    nb_val_samples=5400 // batch_size, callbacks=[early_stop], verbose=2)
model.save_weights('distance.h5')  # always save your weights after training or during training

print train_generator.class_indices
# {'11': 1, '10': 0, '13': 3, '12': 2, '15': 5, '14': 4, '7': 6, '9': 8, '8': 7}
