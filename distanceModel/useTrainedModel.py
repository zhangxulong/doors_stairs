import numpy
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
import numpy as np
import os
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
import cv2

K.set_image_dim_ordering('th')
SEED = 1007
np.random.seed(SEED)
DATA_TEST = "../../data/doors_stairs/dist_test/"
model = Sequential()
model.add(Conv2D(32, 3, 3, input_shape=(3, 150, 150)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(32, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(64, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
# the model so far outputs 3D feature maps (height, width, features)
model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(9))
model.add(Activation('sigmoid'))
model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

batch_size = 4

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

# this is a generator that will read pictures found in
# subfolers of 'data/train', and indefinitely generate
# batches of augmented image data
# this is a similar generator, for validation data
# validation_generator = test_datagen.flow_from_directory(
#     DATA_TEST,
#     target_size=(150, 150),
#     batch_size=batch_size,
#     class_mode='categorical')

model.load_weights('distance.h5')


def distance(img_path):
    # img_path = '../data/doors_stairs/test/doors/door1_1_0000.jpg'
    # img_path = '../data/doors_stairs/test/stairs/stair1_1_0000.jpg'
    img = image.load_img(img_path, target_size=(150, 150))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x=x/255.
    # print numpy.shape(x)
    # print x[0,0,0,20]
    # x = preprocess_input(x)
    preds = model.predict_classes([x], verbose=0)
    # print preds
    # res = model.predict_classes([x])
    res = int(preds[0])
    dict_label = {1: '11ft', 0: '10ft', 3: '13ft', 2: '12ft', 5: '15ft', 4: '14ft', 6: '7ft', 8: '9ft', 7: '8ft'}
    result = dict_label[res]
    return result


def distance_image(raw_image):
    # img_path = '../data/doors_stairs/test/doors/door1_1_0000.jpg'
    # img_path = '../data/doors_stairs/test/stairs/stair1_1_0000.jpg'
    img = cv2.resize(raw_image, (150, 150))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x=x/255.
    # print numpy.shape(x)
    # x = preprocess_input(x)
    preds = model.predict_classes([x], verbose=0)
    # res = model.predict_classes([x])
    res = int(preds[0])
    dict_label = {1: '11ft', 0: '10ft', 3: '13ft', 2: '12ft', 5: '15ft', 4: '14ft', 6: '7ft', 8: '9ft', 7: '8ft'}
    result = dict_label[res]

    return result


# print distance('../../data/doors_stairs/dist_test/7/Stair-3-7ft_0068.jpg')


def batch_predict(data_dir):
    total = 0
    right = 0
    for root, dirs, files in os.walk(data_dir):
        for img_file in files:
            img_path = os.path.join(root, img_file)
            folder = img_path.split('/')[-2]
            if '.jpg' in img_file:
                total += 1
                result = distance(img_path)
                print result
                if folder[:-1] in result:
                    right += 1
    return right / 1.0 / total#0.981118881119


# print batch_predict(DATA_TEST)
    # img_path = '../../data/doors_stairs/dist_test/7/1_1_0000.jpg'
    # print distance(img_path)
