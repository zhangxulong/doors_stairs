import cv2

from getDistance import get_the_distance_image

MOV_DOOR = '../data/doors_stairs/video/door.MOV'
MOV_STAIR = '../data/doors_stairs/video/stair.MOV'


def give_the_mov(mov_path=MOV_STAIR):
    cap = cv2.VideoCapture(mov_path)
    while not cap.isOpened():
        cap = cv2.VideoCapture(mov_path)
        cv2.waitKey(1000)
        print "Wait for the header"

    pos_frame = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
    while True:
        flag, frame = cap.read()
        frame = get_the_distance_image(frame)

        if flag:
            cv2.putText(frame, "%s:%.2fft" % ("door", (5 / 12.0)),
                        (frame.shape[1] - 1250, frame.shape[0] - 250), cv2.FONT_HERSHEY_SIMPLEX,
                        1.0, (0, 255, 255), 2)

            # The frame is ready and already captured
            cv2.imshow('video', frame)

            pos_frame = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
            print str(pos_frame) + " frames"
        else:
            # The next frame is not ready, so we try to read it again
            cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, pos_frame - 1)
            print "frame is not ready"
            # It is better to wait for a while for the next frame to be ready
            cv2.waitKey(1000)

        if cv2.waitKey(10) == 27:
            break
        if cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES) == cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT):
            # If the number of captured frames is equal to the total number of frames,
            # we stop
            break
    return 0


# give_the_mov()

def record_from_webcame():
    cv2.namedWindow("webcam")
    cap = cv2.VideoCapture(0)

    while not cap.isOpened():
        cv2.namedWindow("webcam")
        cap = cv2.VideoCapture(0)
        cv2.waitKey(1000)
        print "Wait for the header"

    pos_frame = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
    while True:
        flag, frame = cap.read()
        frame = get_the_distance_image(frame)

        if flag:
            cv2.putText(frame, "%s:%.2fft" % ("door", (5 / 12.0)),
                        (frame.shape[1] - 1250, frame.shape[0] - 250), cv2.FONT_HERSHEY_SIMPLEX,
                        1.0, (0, 255, 255), 2)

            # The frame is ready and already captured
            cv2.imshow('video', frame)

            pos_frame = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
            print str(pos_frame) + " frames"
        else:
            # The next frame is not ready, so we try to read it again
            cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, pos_frame - 1)
            print "frame is not ready"
            # It is better to wait for a while for the next frame to be ready
            cv2.waitKey(1000)

        if cv2.waitKey(10) == 27:
            break
            cv2.destroyWindow("webcam")
        if cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES) == cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT):
            # If the number of captured frames is equal to the total number of frames,
            # we stop
            break
            cv2.destroyWindow("webcam")
    return 0

    # give_the_mov()
