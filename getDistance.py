# import the necessary packages
import numpy as np
import cv2

from distanceModel.useTrainedModel import distance_image
from useModel import door_stair_image


def find_marker(image):
    # COLOR_MIN=np.array([18,18,18],np.uint8)
    # COLOR_MAX = np.array([60,60,50], np.uint8)
    # image=cv2.inRange(image,COLOR_MIN,COLOR_MAX)
    # cv2.imshow("image", image)
    # cv2.waitKey(0)
    # convert the image to grayscale, blur it, and detect edges
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # gray = cv2.GaussianBlur(gray, (7, 7), 0)
    edged = cv2.Canny(gray, 65, 125)
    # cv2.imshow("image", edged)
    # cv2.waitKey(0)

    # find the contours in the edged image and keep the largest one;
    # we'll assume that this is our piece of paper in the image
    (cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # print len(cnts)
    c = max(cnts, key=cv2.contourArea)

    # compute the bounding box of the of the paper region and return it

    return cv2.minAreaRect(c)


def distance_to_camera(knownWidth, focalLength, perWidth):
    # compute and return the distance from the maker to the camera
    return (knownWidth * focalLength) / perWidth


def get_the_distance(imagePath="../data/doors_stairs/cv2/door1_4_0000.jpg"):
    # initialize the known distance from the camera to the object, which
    # in this case is 24 inches
    KNOWN_DISTANCE = 180.0
    # initialize the known object width, which in this case, the piece of
    # paper is 12 inches wide
    KNOWN_WIDTH = 78.0

    # initialize the list of images that we'll be using
    IMAGE_PATHS = ["../data/doors_stairs/cv2/door1_1_0000.jpg", "../data/doors_stairs/cv2/door1_2_0000.jpg",
                   "../data/doors_stairs/cv2/door1_3_0000.jpg", "../data/doors_stairs/cv2/door1_4_0000.jpg"]

    # load the first image that contains an object that is KNOWN TO BE 2 feet
    # from our camera, then find the paper marker in the image, and initialize
    # the focal length
    image = cv2.imread(IMAGE_PATHS[0])
    image = cv2.resize(image, (300, 300))
    marker = find_marker(image)
    # print marker
    focalLength = (marker[1][0] * KNOWN_DISTANCE) / KNOWN_WIDTH
    # print focalLength
    # loop over the images

    # load the image, find the marker in the image, then compute the
    # distance to the marker from the camera
    image = cv2.imread(imagePath)
    image = cv2.resize(image, (300, 300))
    marker = find_marker(image)
    inches = distance_to_camera(KNOWN_WIDTH, focalLength, marker[1][0])
    is_door_stair = door_stair_image(imagePath)

    # draw a bounding box around the image and display it
    box = np.int0(cv2.cv.BoxPoints(marker))
    cv2.drawContours(image, [box], -1, (0, 255, 0), 2)
    cv2.putText(image, "%s:%.2fft" % (is_door_stair, (inches / 12.0)),
                (image.shape[1] - 250, image.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX,
                1.0, (0, 255, 255), 2)
    cv2.imshow("image", image)
    cv2.waitKey(0)
    return inches / 12.0


    # print get_the_distance()


def get_the_distance_image(image):
    image = cv2.resize(image, (600, 600))
    is_door_stair = door_stair_image(image)
    distance = distance_image(image)
    if is_door_stair == 'door':
        # initialize the known distance from the camera to the object, which
        # in this case is 24 inches
        KNOWN_DISTANCE = 180.0
        # initialize the known object width, which in this case, the piece of
        # paper is 12 inches wide
        KNOWN_WIDTH = 78.0
    else:
        KNOWN_DISTANCE = 180.0
        KNOWN_WIDTH = 59.0
    # initialize the list of images that we'll be using
    # load the first image that contains an object that is KNOWN TO BE 2 feet
    # from our camera, then find the paper marker in the image, and initialize
    # the focal length
    # image_first = cv2.imread("../data/doors_stairs/cv2/door1_1_0000.jpg")
    # image_first = cv2.resize(image_first, (300, 300))
    # marker = find_marker(image_first)
    # print marker
    # focalLength = (marker[1][0] * KNOWN_DISTANCE) / KNOWN_WIDTH
    # print focalLength
    # loop over the images
    # load the image, find the marker in the image, then compute the
    # distance to the marker from the camera
    marker = find_marker(image)
    # inches = distance_to_camera(KNOWN_WIDTH, focalLength, marker[1][0])# TODO replaced
    inches = distance

    # draw a bounding box around the image and display it
    box = np.int0(cv2.cv.BoxPoints(marker))
    print box
    text_point_x = int((box[0, 0] + box[2, 0]) / 2.0)
    text_point_y = int((box[0, 1] + box[2, 1]) / 2.0)
    print text_point_x, text_point_y
    # cv2.drawContours(image, [box], -1, (0, 255, 0), 2)
    cv2.putText(image, "%s@%s" % (is_door_stair, inches),
                (text_point_x, text_point_y), cv2.FONT_HERSHEY_SIMPLEX,
                1.0, (0, 255, 255), 1)
    # cv2.putText(image, "%s:%.2fft" % (is_door_stair, (inches / 12.0)),
    #             (image.shape[1] - 250, image.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX,
    #             1.0, (0, 255, 255), 2)

    return image
