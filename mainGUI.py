# coding=utf-8
import os
import time
import tkFileDialog
from Tkinter import *

from main import give_the_mov, record_from_webcame


def if_no_create_it(file_path):
    the_dir = os.path.dirname(file_path)
    if os.path.isdir(the_dir):
        pass
    else:
        os.makedirs(the_dir)


def nowTimeStr():
    secs = time.time()
    return time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime(secs))


def mainFunction():
    def detect_the_video():
        timestamp = nowTimeStr()
        video_path = link_contend_gif.get()
        video_format = video_path.split('.')[-1]
        new_video_path = timestamp + '.' + video_format
        print new_video_path
        os.rename(video_path, new_video_path)
        give_the_mov(new_video_path)
        os.rename(new_video_path, video_path)

        return 0

    def detect_the_webcame():
        record_from_webcame()
        return 0

    def choose():
        filename = tkFileDialog.askopenfilename(initialdir='/home/zhangxulong')
        link_contend_gif.set(filename)
        return 0

    root = Tk()
    root.title('door and stair detection')

    labe_txt_gif = Label(root, text='choose video file：')
    labe_txt_gif.grid(row=1, column=0)
    entry_link_gif = Entry(root, width=40)
    entry_link_gif.grid(row=1, column=1)
    link_contend_gif = StringVar()
    entry_link_gif.config(textvariable=link_contend_gif)
    link_contend_gif.set('')
    choose_button = Button(root, text='choose', command=choose)
    choose_button.grid(row=1, column=2)
    gif_button = Button(root, text='detect', command=detect_the_video)
    gif_button.grid(row=1, column=3)
    gif_button = Button(root, text='webcame', command=detect_the_webcame)
    gif_button.grid(row=1, column=4)

    mainloop()
    return 0


mainFunction()
